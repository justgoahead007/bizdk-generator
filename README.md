# bizdk-generator

#### 介绍
BIZDK 代码生成器,无关框架,可以根据模板生成代码, 目前可以生成单表curd代码
目前支持主流数据库

#### 变量
每个模板上都会传入一个上下文，上下文信息如下：

``` json
{
    "date": "2023-04-23",
    "ModuleName": "User",
    "tableName": "user",
    "queryList": [],
    "FunctionName": "User",
    "datetime": "2023-04-23 23:29:05",
    "beanName": "user",
    "email": "823604769@qq.com",
    "packagePath": "com\\bizdk",
    "package": "com.bizdk",
    "functionName": "user",
    "author": "liqiaowen",
    "BeanName": "User",
    "dbType": "MySQL",
    "tableComment": "用户表",
    "version": "v1",
    "formLayout": 1,	
    "moduleName": "user",
    "templateName": "sql/menu.sql.ftl",	
    "columnList": [{
            "id": 1649733768429719553,
            "tableId": 1649733768224198657,
            "columnName": "user_id",
            "columnType": "bigint",
            "columnComment": "ID",
            "primaryPk": true,
            "sort": 0,
            "attrName": "userId",
            "attrType": "Long",
            "packageName": null,
            "autoFill": "DEFAULT",
            "baseAttr": false,
            "formItem": true,
            "formRequired": false,
            "formType": "text",
            "formDict": null,
            "formValidator": null,
            "gridItem": true,
            "gridSort": false,
            "queryItem": false,
            "queryType": "=",
            "queryFormType": "text"
        }
    ],
    "gridList": [{
            "id": 1649733768429719553,
            "tableId": 1649733768224198657,
            "columnName": "user_id",
            "columnType": "bigint",
            "columnComment": "ID",
            "primaryPk": true,
            "sort": 0,
            "attrName": "userId",
            "attrType": "Long",
            "packageName": null,
            "autoFill": "DEFAULT",
            "baseAttr": false,
            "formItem": true,
            "formRequired": false,
            "formType": "text",
            "formDict": null,
            "formValidator": null,
            "gridItem": true,
            "gridSort": false,
            "queryItem": false,
            "queryType": "=",
            "queryFormType": "text"
        }
    ],
    "primaryList": [{
            "id": 1649733768429719553,
            "tableId": 1649733768224198657,
            "columnName": "user_id",
            "columnType": "bigint",
            "columnComment": "ID",
            "primaryPk": true,
            "sort": 0,
            "attrName": "userId",
            "attrType": "Long",
            "packageName": null,
            "autoFill": "DEFAULT",
            "baseAttr": false,
            "formItem": true,
            "formRequired": false,
            "formType": "text",
            "formDict": null,
            "formValidator": null,
            "gridItem": true,
            "gridSort": false,
            "queryItem": false,
            "queryType": "=",
            "queryFormType": "text"
        }
    ],
    "importList": ["java.util.Date"],
    "formList": [{
            "id": 1649733768429719553,
            "tableId": 1649733768224198657,
            "columnName": "user_id",
            "columnType": "bigint",
            "columnComment": "ID",
            "primaryPk": true,
            "sort": 0,
            "attrName": "userId",
            "attrType": "Long",
            "packageName": null,
            "autoFill": "DEFAULT",
            "baseAttr": false,
            "formItem": true,
            "formRequired": false,
            "formType": "text",
            "formDict": null,
            "formValidator": null,
            "gridItem": true,
            "gridSort": false,
            "queryItem": false,
            "queryType": "=",
            "queryFormType": "text"
        }
    ],
    "frontendPath": "D:/source/bizdk-generator/demo/frontend",
    "backendPath": "D:/source/bizdk-generator/demo/backend"
}

```


### 代码生成

### 模板编写


模板示例：
``` java


```


