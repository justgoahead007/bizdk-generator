var bizdk = {
    pageResponseHandler: function(res){
        if (res.data){
            res.rows = res.data.list;
            res.total = res.data.total;
        } else {
            res.rows = [];
            res.total = 0;
        }
    },
    listResponseHandler: function(res){
        if (res.data){
            res.rows = res.data;
            res.total = res.data.length;
        } else {
            res.rows = [];
            res.total = 0;
        }
    }
}