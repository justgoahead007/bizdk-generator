-- --------------------------------------------------------
-- 主机:                           D:\source\bizdk-generator\db\bizdk_generator.sqlite3
-- 服务器版本:                        3.39.4
-- 服务器操作系统:                      
-- HeidiSQL 版本:                  12.4.0.6659
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES  */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- 导出 bizdk_generator 的数据库结构
CREATE DATABASE IF NOT EXISTS "bizdk_generator";
;

-- 导出  表 bizdk_generator.bd_base_class 结构
DROP TABLE IF EXISTS "bd_base_class";
CREATE TABLE IF NOT EXISTS "bd_base_class" (
	"id" BIGINT NOT NULL,
	"package_name" VARCHAR(200) NULL,
	"code" VARCHAR(200) NULL,
	"attrs" VARCHAR(500) NULL,
	"remark" VARCHAR(200) NULL,
	"create_time" DATETIME NULL,
	PRIMARY KEY (`id`)  
);

-- 正在导出表  bizdk_generator.bd_base_class 的数据：1 rows
/*!40000 ALTER TABLE "bd_base_class" DISABLE KEYS */;
INSERT INTO "bd_base_class" ("id", "package_name", "code", "attrs", "remark", "create_time") VALUES
	(1641096140242849793, 'com.bizdk.common.entity', 'BaseEntity', 'id,creator,create_time,updater,update_time,version,deleted', '实体基类', '2023-03-29 23:13:05');
/*!40000 ALTER TABLE "bd_base_class" ENABLE KEYS */;

-- 导出  表 bizdk_generator.bd_datasource 结构
DROP TABLE IF EXISTS "bd_datasource";
CREATE TABLE IF NOT EXISTS "bd_datasource" (
	"id" BIGINT NOT NULL,
	"db_type" VARCHAR(200) NULL,
	"conn_name" VARCHAR(200) NOT NULL,
	"conn_url" VARCHAR(500) NULL,
	"username" VARCHAR(200) NULL,
	"password" VARCHAR(200) NULL,
	"create_time" DATETIME NULL,
	PRIMARY KEY ("id")
);

-- 正在导出表  bizdk_generator.bd_datasource 的数据：-1 rows
/*!40000 ALTER TABLE "bd_datasource" DISABLE KEYS */;
/*!40000 ALTER TABLE "bd_datasource" ENABLE KEYS */;

-- 导出  表 bizdk_generator.bd_table 结构
DROP TABLE IF EXISTS "bd_table";
CREATE TABLE IF NOT EXISTS `bd_table`  (
  `id` bigint NOT NULL ,
  `table_name` varchar(200)  NULL DEFAULT NULL,
  `bean_name` varchar(200)  NULL DEFAULT NULL ,
  `table_comment` varchar(200)  NULL DEFAULT NULL ,
  `function_name` varchar(200)  NULL DEFAULT NULL ,
  `form_layout` tinyint NULL DEFAULT NULL  ,
  `datasource_id` bigint NULL DEFAULT NULL  ,
  `baseclass_id` bigint NULL DEFAULT NULL  ,
  `create_time` datetime NULL DEFAULT NULL  ,
  PRIMARY KEY (`id`) 
);

-- 正在导出表  bizdk_generator.bd_table 的数据：-1 rows
/*!40000 ALTER TABLE "bd_table" DISABLE KEYS */;
/*!40000 ALTER TABLE "bd_table" ENABLE KEYS */;

-- 导出  表 bizdk_generator.bd_table_column 结构
DROP TABLE IF EXISTS "bd_table_column";
CREATE TABLE IF NOT EXISTS `bd_table_column`  (
  `id` bigint NOT NULL ,
  `table_id` bigint NULL DEFAULT NULL,
  `column_name` varchar(200)  NULL DEFAULT NULL ,
  `column_type` varchar(200)  NULL DEFAULT NULL ,
  `column_comment` varchar(200)  NULL DEFAULT NULL ,
  `attr_name` varchar(200)  NULL DEFAULT NULL ,
  `attr_type` varchar(200)  NULL DEFAULT NULL ,
  `package_name` varchar(200)  NULL DEFAULT NULL ,
  `sort` int NULL DEFAULT NULL ,
  `auto_fill` varchar(20)  NULL DEFAULT NULL ,
  `primary_pk` tinyint NULL DEFAULT NULL ,
  `base_attr` tinyint NULL DEFAULT NULL ,
  `form_item` tinyint NULL DEFAULT NULL ,
  `form_required` tinyint NULL DEFAULT NULL ,
  `form_type` varchar(200)  NULL DEFAULT NULL ,
  `form_dict` varchar(200)  NULL DEFAULT NULL ,
  `form_validator` varchar(200)  NULL DEFAULT NULL,
  `grid_item` tinyint NULL DEFAULT NULL ,
  `grid_sort` tinyint NULL DEFAULT NULL ,
  `query_item` tinyint NULL DEFAULT NULL ,
  `query_type` varchar(200)  NULL DEFAULT NULL ,
  `query_form_type` varchar(200)  NULL DEFAULT NULL ,
  PRIMARY KEY (`id`)
);

-- 正在导出表  bizdk_generator.bd_table_column 的数据：-1 rows
/*!40000 ALTER TABLE "bd_table_column" DISABLE KEYS */;
/*!40000 ALTER TABLE "bd_table_column" ENABLE KEYS */;

-- 导出  表 bizdk_generator.bd_template 结构
DROP TABLE IF EXISTS "bd_template";
CREATE TABLE IF NOT EXISTS `bd_template`  (
  `id` bigint NOT NULL ,
  `project_name` varchar(100)  NULL DEFAULT NULL,
  `project_code` varchar(100)  NULL DEFAULT NULL ,
  `project_package` varchar(100)  NULL DEFAULT NULL ,
  `project_path` varchar(200)  NULL DEFAULT NULL ,
  `modify_project_name` varchar(100)  NULL DEFAULT NULL ,
  `modify_project_code` varchar(100)  NULL DEFAULT NULL ,
  `modify_project_package` varchar(100)  NULL DEFAULT NULL,
  `exclusions` varchar(200)  NULL DEFAULT NULL ,
  `modify_suffix` varchar(200)  NULL DEFAULT NULL,
  `modify_tmp_path` varchar(100)  NULL DEFAULT NULL ,
  `create_time` datetime NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) 
);

-- 正在导出表  bizdk_generator.bd_template 的数据：-1 rows
/*!40000 ALTER TABLE "bd_template" DISABLE KEYS */;
/*!40000 ALTER TABLE "bd_template" ENABLE KEYS */;

-- 导出  表 bizdk_generator.bd_type_mapping 结构
DROP TABLE IF EXISTS "bd_type_mapping";
CREATE TABLE IF NOT EXISTS `bd_type_mapping`  (
  `id` bigint NOT NULL ,
  `column_type` varchar(200)  NULL DEFAULT NULL ,
  `attr_type` varchar(200)  NULL DEFAULT NULL ,
  `package_name` varchar(200)  NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) 
);

-- 正在导出表  bizdk_generator.bd_type_mapping 的数据：-1 rows
/*!40000 ALTER TABLE "bd_type_mapping" DISABLE KEYS */;
INSERT INTO "bd_type_mapping" ("id", "column_type", "attr_type", "package_name", "create_time") VALUES
	(1641096140242849701, 'datetime', 'Date', 'java.util.Date', '2023-03-18 17:03:57'),
	(1641096140242849702, 'date', 'Date', 'java.util.Date', '2023-03-18 17:03:57'),
	(1641096140242849703, 'tinyint', 'Integer', NULL, '2023-03-18 17:03:57'),
	(1641096140242849704, 'smallint', 'Integer', NULL, '2023-03-18 17:03:57'),
	(1641096140242849705, 'mediumint', 'Integer', NULL, '2023-03-18 17:03:57'),
	(1641096140242849706, 'int', 'Integer', NULL, '2023-03-18 17:03:57'),
	(1641096140242849707, 'integer', 'Integer', NULL, '2023-03-18 17:03:57'),
	(1641096140242849708, 'bigint', 'Long', NULL, '2023-03-18 17:03:57'),
	(1641096140242849709, 'float', 'Float', NULL, '2023-03-18 17:03:57'),
	(1641096140242849710, 'double', 'Double', NULL, '2023-03-18 17:03:57'),
	(1641096140242849711, 'decimal', 'BigDecimal', 'java.math.BigDecimal', '2023-03-18 17:03:57'),
	(1641096140242849712, 'bit', 'Boolean', NULL, '2023-03-18 17:03:58'),
	(1641096140242849713, 'char', 'String', NULL, '2023-03-18 17:03:58'),
	(1641096140242849714, 'varchar', 'String', NULL, '2023-03-18 17:03:58'),
	(1641096140242849715, 'tinytext', 'String', NULL, '2023-03-18 17:03:58'),
	(1641096140242849716, 'text', 'String', NULL, '2023-03-18 17:03:58'),
	(1641096140242849717, 'mediumtext', 'String', NULL, '2023-03-18 17:03:58'),
	(1641096140242849718, 'longtext', 'String', NULL, '2023-03-18 17:03:58'),
	(1641096140242849719, 'timestamp', 'Date', 'java.util.Date', '2023-03-18 17:03:58'),
	(1641096140242849720, 'NUMBER', 'Integer', NULL, '2023-03-18 17:03:58'),
	(1641096140242849721, 'BINARY_INTEGER', 'Integer', NULL, '2023-03-18 17:03:58'),
	(1641096140242849722, 'BINARY_FLOAT', 'Float', NULL, '2023-03-18 17:03:58'),
	(1641096140242849723, 'BINARY_DOUBLE', 'Double', NULL, '2023-03-18 17:03:58'),
	(1641096140242849724, 'VARCHAR2', 'String', NULL, '2023-03-18 17:03:58'),
	(1641096140242849725, 'NVARCHAR', 'String', NULL, '2023-03-18 17:03:58'),
	(1641096140242849726, 'NVARCHAR2', 'String', NULL, '2023-03-18 17:03:58'),
	(1641096140242849727, 'CLOB', 'String', NULL, '2023-03-18 17:03:58'),
	(1641096140242849728, 'int8', 'Long', NULL, '2023-03-18 17:03:58'),
	(1641096140242849729, 'int4', 'Integer', NULL, '2023-03-18 17:03:58'),
	(1641096140242849730, 'int2', 'Integer', NULL, '2023-03-18 17:03:58'),
	(1641096140242849731, 'numeric', 'BigDecimal', 'java.math.BigDecimal', '2023-03-18 17:03:58');
/*!40000 ALTER TABLE "bd_type_mapping" ENABLE KEYS */;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
