/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : bizdk_generator

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 29/03/2023 23:21:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bd_base_class
-- ----------------------------
DROP TABLE IF EXISTS `bd_base_class`;
CREATE TABLE `bd_base_class`  (
  `id` bigint NOT NULL COMMENT 'id',
  `package_name` varchar(200)  NULL DEFAULT NULL COMMENT '基类包名',
  `code` varchar(200)  NULL DEFAULT NULL COMMENT '基类编码',
  `attrs` varchar(500)  NULL DEFAULT NULL COMMENT '基类字段，多个用英文逗号分隔',
  `remark` varchar(200)  NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB COMMENT = '基类管理' ;

-- ----------------------------
-- Records of bd_base_class
-- ----------------------------
INSERT INTO `bd_base_class` VALUES (1641096140242849793, 'com.bizdk.common.entity', 'BaseEntity', 'id,creator,create_time,updater,update_time,version,deleted', '实体基类', '2023-03-29 23:13:05');

-- ----------------------------
-- Table structure for bd_datasource
-- ----------------------------
DROP TABLE IF EXISTS `bd_datasource`;
CREATE TABLE `bd_datasource`  (
  `id` bigint NOT NULL COMMENT 'id',
  `db_type` varchar(200)  NULL DEFAULT NULL COMMENT '数据库类型',
  `conn_name` varchar(200)  NOT NULL COMMENT '连接名',
  `conn_url` varchar(500)  NULL DEFAULT NULL COMMENT 'URL',
  `username` varchar(200)  NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(200)  NULL DEFAULT NULL COMMENT '密码',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB COMMENT = '数据源管理' ;

-- ----------------------------
-- Records of bd_datasource
-- ----------------------------

-- ----------------------------
-- Table structure for bd_table
-- ----------------------------
DROP TABLE IF EXISTS `bd_table`;
CREATE TABLE `bd_table`  (
  `id` bigint NOT NULL COMMENT 'id',
  `table_name` varchar(200)  NULL DEFAULT NULL COMMENT '表名',
  `bean_name` varchar(200)  NULL DEFAULT NULL COMMENT '类名',
  `table_comment` varchar(200)  NULL DEFAULT NULL COMMENT '说明',
  `function_name` varchar(200)  NULL DEFAULT NULL COMMENT '功能名',
  `form_layout` tinyint NULL DEFAULT NULL COMMENT '表单布局  1：一列   2：两列',
  `datasource_id` bigint NULL DEFAULT NULL COMMENT '数据源ID',
  `baseclass_id` bigint NULL DEFAULT NULL COMMENT '基类ID',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `table_name`(`table_name` ASC) USING BTREE
) ENGINE = InnoDB COMMENT = '代码生成表' ;

-- ----------------------------
-- Records of bd_table
-- ----------------------------

-- ----------------------------
-- Table structure for bd_table_column
-- ----------------------------
DROP TABLE IF EXISTS `bd_table_column`;
CREATE TABLE `bd_table_column`  (
  `id` bigint NOT NULL COMMENT 'id',
  `table_id` bigint NULL DEFAULT NULL COMMENT '表ID',
  `column_name` varchar(200)  NULL DEFAULT NULL COMMENT '字段名称',
  `column_type` varchar(200)  NULL DEFAULT NULL COMMENT '字段类型',
  `column_comment` varchar(200)  NULL DEFAULT NULL COMMENT '字段说明',
  `attr_name` varchar(200)  NULL DEFAULT NULL COMMENT '属性名',
  `attr_type` varchar(200)  NULL DEFAULT NULL COMMENT '属性类型',
  `package_name` varchar(200)  NULL DEFAULT NULL COMMENT '属性包名',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `auto_fill` varchar(20)  NULL DEFAULT NULL COMMENT '自动填充  DEFAULT、INSERT、UPDATE、INSERT_UPDATE',
  `primary_pk` tinyint NULL DEFAULT NULL COMMENT '主键 0：否  1：是',
  `base_attr` tinyint NULL DEFAULT NULL COMMENT '基类字段 0：否  1：是',
  `form_item` tinyint NULL DEFAULT NULL COMMENT '表单项 0：否  1：是',
  `form_required` tinyint NULL DEFAULT NULL COMMENT '表单必填 0：否  1：是',
  `form_type` varchar(200)  NULL DEFAULT NULL COMMENT '表单类型',
  `form_dict` varchar(200)  NULL DEFAULT NULL COMMENT '表单字典类型',
  `form_validator` varchar(200)  NULL DEFAULT NULL COMMENT '表单效验',
  `grid_item` tinyint NULL DEFAULT NULL COMMENT '列表项 0：否  1：是',
  `grid_sort` tinyint NULL DEFAULT NULL COMMENT '列表排序 0：否  1：是',
  `query_item` tinyint NULL DEFAULT NULL COMMENT '查询项 0：否  1：是',
  `query_type` varchar(200)  NULL DEFAULT NULL COMMENT '查询方式',
  `query_form_type` varchar(200)  NULL DEFAULT NULL COMMENT '查询表单类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB COMMENT = '代码生成表字段' ;

-- ----------------------------
-- Records of bd_table_column
-- ----------------------------

-- ----------------------------
-- Table structure for bd_template
-- ----------------------------
DROP TABLE IF EXISTS `bd_template`;
CREATE TABLE `bd_template`  (
  `id` bigint NOT NULL COMMENT 'id',
  `project_name` varchar(100)  NULL DEFAULT NULL COMMENT '项目名',
  `project_code` varchar(100)  NULL DEFAULT NULL COMMENT '项目标识',
  `project_package` varchar(100)  NULL DEFAULT NULL COMMENT '项目包名',
  `project_path` varchar(200)  NULL DEFAULT NULL COMMENT '项目路径',
  `modify_project_name` varchar(100)  NULL DEFAULT NULL COMMENT '变更项目名',
  `modify_project_code` varchar(100)  NULL DEFAULT NULL COMMENT '变更标识',
  `modify_project_package` varchar(100)  NULL DEFAULT NULL COMMENT '变更包名',
  `exclusions` varchar(200)  NULL DEFAULT NULL COMMENT '排除文件',
  `modify_suffix` varchar(200)  NULL DEFAULT NULL COMMENT '变更文件',
  `modify_tmp_path` varchar(100)  NULL DEFAULT NULL COMMENT '变更临时路径',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB COMMENT = '项目名变更' ;

-- ----------------------------
-- Records of bd_template
-- ----------------------------

-- ----------------------------
-- Table structure for bd_type_mapping
-- ----------------------------
DROP TABLE IF EXISTS `bd_type_mapping`;
CREATE TABLE `bd_type_mapping`  (
  `id` bigint NOT NULL COMMENT 'id',
  `column_type` varchar(200)  NULL DEFAULT NULL COMMENT '字段类型',
  `attr_type` varchar(200)  NULL DEFAULT NULL COMMENT '属性类型',
  `package_name` varchar(200)  NULL DEFAULT NULL COMMENT '属性包名',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `column_type`(`column_type` ASC) USING BTREE
) ENGINE = InnoDB COMMENT = '字段类型管理' ;

-- ----------------------------
-- Records of bd_type_mapping
-- ----------------------------
INSERT INTO `bd_type_mapping` VALUES (1641096140242849701, 'datetime', 'Date', 'java.util.Date', '2023-03-18 17:03:57');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849702, 'date', 'Date', 'java.util.Date', '2023-03-18 17:03:57');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849703, 'tinyint', 'Integer', NULL, '2023-03-18 17:03:57');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849704, 'smallint', 'Integer', NULL, '2023-03-18 17:03:57');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849705, 'mediumint', 'Integer', NULL, '2023-03-18 17:03:57');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849706, 'int', 'Integer', NULL, '2023-03-18 17:03:57');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849707, 'integer', 'Integer', NULL, '2023-03-18 17:03:57');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849708, 'bigint', 'Long', NULL, '2023-03-18 17:03:57');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849709, 'float', 'Float', NULL, '2023-03-18 17:03:57');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849710, 'double', 'Double', NULL, '2023-03-18 17:03:57');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849711, 'decimal', 'BigDecimal', 'java.math.BigDecimal', '2023-03-18 17:03:57');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849712, 'bit', 'Boolean', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849713, 'char', 'String', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849714, 'varchar', 'String', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849715, 'tinytext', 'String', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849716, 'text', 'String', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849717, 'mediumtext', 'String', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849718, 'longtext', 'String', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849719, 'timestamp', 'Date', 'java.util.Date', '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849720, 'NUMBER', 'Integer', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849721, 'BINARY_INTEGER', 'Integer', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849722, 'BINARY_FLOAT', 'Float', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849723, 'BINARY_DOUBLE', 'Double', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849724, 'VARCHAR2', 'String', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849725, 'NVARCHAR', 'String', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849726, 'NVARCHAR2', 'String', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849727, 'CLOB', 'String', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849728, 'int8', 'Long', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849729, 'int4', 'Integer', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849730, 'int2', 'Integer', NULL, '2023-03-18 17:03:58');
INSERT INTO `bd_type_mapping` VALUES (1641096140242849731, 'numeric', 'BigDecimal', 'java.math.BigDecimal', '2023-03-18 17:03:58');

SET FOREIGN_KEY_CHECKS = 1;
