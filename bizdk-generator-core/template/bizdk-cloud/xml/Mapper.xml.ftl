<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="${package}.${moduleName}.mapper.${BeanName}Mapper">

    <resultMap type="${package}.${moduleName}.entity.${BeanName}Entity" id="${beanName}Map">
        <#list columnList as column>
        <result property="${column.attrName}" column="${column.columnName}"/>
        </#list>
    </resultMap>

</mapper>