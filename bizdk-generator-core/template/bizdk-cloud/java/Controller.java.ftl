package ${package}.${moduleName}.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import ${package}.framework.common.utils.PageResult;
import ${package}.framework.common.utils.Result;
import ${package}.${moduleName}.convert.${BeanName}Convert;
import ${package}.${moduleName}.entity.${BeanName}Entity;
import ${package}.${moduleName}.service.${BeanName}Service;
import ${package}.${moduleName}.query.${BeanName}Query;
import ${package}.${moduleName}.vo.${BeanName}VO;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.List;

/**
* ${tableComment}
*
* @author ${author} ${email}
* @since ${version} ${date}
*/
@RestController
@RequestMapping("${functionName}")
@Tag(name="${tableComment}")
@AllArgsConstructor
public class ${BeanName}Controller {
    private final ${BeanName}Service ${beanName}Service;

    @GetMapping("page")
    @Operation(summary = "分页")
    @PreAuthorize("hasAuthority('${moduleName}:${functionName}:page')")
    public Result<PageResult<${BeanName}VO>> page(@ParameterObject @Valid ${BeanName}Query query){
        PageResult<${BeanName}VO> page = ${beanName}Service.page(query);

        return Result.ok(page);
    }

    @GetMapping("{id}")
    @Operation(summary = "信息")
    @PreAuthorize("hasAuthority('${moduleName}:${functionName}:info')")
    public Result<${BeanName}VO> get(@PathVariable("id") Long id){
        ${BeanName}Entity entity = ${beanName}Service.getById(id);

        return Result.ok(${BeanName}Convert.INSTANCE.convert(entity));
    }

    @PostMapping
    @Operation(summary = "保存")
    @PreAuthorize("hasAuthority('${moduleName}:${functionName}:save')")
    public Result<String> save(@RequestBody ${BeanName}VO vo){
        ${beanName}Service.save(vo);

        return Result.ok();
    }

    @PutMapping
    @Operation(summary = "修改")
    @PreAuthorize("hasAuthority('${moduleName}:${functionName}:update')")
    public Result<String> update(@RequestBody @Valid ${BeanName}VO vo){
        ${beanName}Service.update(vo);

        return Result.ok();
    }

    @DeleteMapping
    @Operation(summary = "删除")
    @PreAuthorize("hasAuthority('${moduleName}:${functionName}:delete')")
    public Result<String> delete(@RequestBody List<Long> idList){
        ${beanName}Service.delete(idList);

        return Result.ok();
    }
}