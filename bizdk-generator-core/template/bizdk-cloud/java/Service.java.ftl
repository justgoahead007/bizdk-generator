package ${package}.${moduleName}.service;

import com.baomidou.mybatisplus.extension.service.IService;
import ${package}.framework.common.utils.PageResult;
import ${package}.${moduleName}.vo.${BeanName}VO;
import ${package}.${moduleName}.query.${BeanName}Query;
import ${package}.${moduleName}.entity.${BeanName}Entity;

import java.util.List;

/**
 * ${tableComment}
 *
 * @author ${author} ${email}
 * @since ${version} ${date}
 */
public interface ${BeanName}Service extends IService<${BeanName}Entity> {

    PageResult<${BeanName}VO> page(${BeanName}Query query);

    void save(${BeanName}VO vo);

    void update(${BeanName}VO vo);

    void delete(List<Long> idList);
}