package ${package}.${moduleName}.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ${package}.framework.common.utils.PageResult;
import ${package}.${moduleName}.convert.${BeanName}Convert;
import ${package}.${moduleName}.entity.${BeanName}Entity;
import ${package}.${moduleName}.query.${BeanName}Query;
import ${package}.${moduleName}.vo.${BeanName}VO;
import ${package}.${moduleName}.mapper.${BeanName}Mapper;
import ${package}.${moduleName}.service.${BeanName}Service;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * ${tableComment}
 *
 * @author ${author} ${email}
 * @since ${version} ${date}
 */
@Service
@AllArgsConstructor
public class ${BeanName}ServiceImpl extends ServiceImpl<${BeanName}Mapper, ${BeanName}Entity> implements ${BeanName}Service {

    @Override
    public PageResult<${BeanName}VO> page(${BeanName}Query query) {
        IPage<${BeanName}Entity> page = baseMapper.selectPage(getPage(query), getWrapper(query));

        return new PageResult<>(${BeanName}Convert.INSTANCE.convertList(page.getRecords()), page.getTotal());
    }

    private LambdaQueryWrapper<${BeanName}Entity> getWrapper(${BeanName}Query query){
        LambdaQueryWrapper<${BeanName}Entity> wrapper = Wrappers.lambdaQuery();

        return wrapper;
    }

    @Override
    public void save(${BeanName}VO vo) {
        ${BeanName}Entity entity = ${BeanName}Convert.INSTANCE.convert(vo);

        baseMapper.insert(entity);
    }

    @Override
    public void update(${BeanName}VO vo) {
        ${BeanName}Entity entity = ${BeanName}Convert.INSTANCE.convert(vo);

        updateById(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> idList) {
        removeByIds(idList);
    }

}