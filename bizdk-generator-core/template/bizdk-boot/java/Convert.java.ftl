package ${package}.${moduleName}.convert;

import ${package}.${moduleName}.entity.${BeanName}Entity;
import ${package}.${moduleName}.vo.${BeanName}VO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* ${tableComment}
*
* @author ${author} ${email}
* @since ${version} ${date}
*/
@Mapper
public interface ${BeanName}Convert {
    ${BeanName}Convert INSTANCE = Mappers.getMapper(${BeanName}Convert.class);

    ${BeanName}Entity convert(${BeanName}VO vo);

    ${BeanName}VO convert(${BeanName}Entity entity);

    List<${BeanName}VO> convertList(List<${BeanName}Entity> list);

}