package ${package}.${moduleName}.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import com.baomidou.mybatisplus.annotation.*;
<#list importList as i>
import ${i!};
</#list>
<#if baseClass??>
import ${baseClass.packageName}.${baseClass.code};
</#if>

/**
 * ${tableComment}
 *
 * @author ${author} ${email}
 * @since ${version} ${date}
 */
<#if baseClass??>@EqualsAndHashCode(callSuper=false)</#if>
@Data
@TableName("${tableName}")
public class ${BeanName}Entity<#if baseClass??> extends ${baseClass.code}</#if> {
<#list columnList as column>
<#if !column.baseAttr>
	<#if column.columnComment!?length gt 0>
	/**
	* ${column.columnComment}
	*/
	</#if>
    <#if column.autoFill == "INSERT">
	@TableField(fill = FieldFill.INSERT)
	</#if>
	<#if column.autoFill == "INSERT_UPDATE">
	@TableField(fill = FieldFill.INSERT_UPDATE)
	</#if>
	<#if column.autoFill == "UPDATE">
		@TableField(fill = FieldFill.UPDATE)
	</#if>
    <#if column.primaryPk>
	@TableId
	</#if>
	private ${column.attrType} ${column.attrName};
</#if>

</#list>
}