package com.bizdk.generator.service.impl;

import com.bizdk.generator.GeneratorCoreApp;
import com.bizdk.generator.entity.DataSourceEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest(classes = GeneratorCoreApp.class)
public class DataSourceServiceImplTest {

    @Autowired
    DataSourceServiceImpl dataSourceService;

    @Test
    public void page() {
        List<DataSourceEntity> list = dataSourceService.getList();
        System.out.println("==========================" + list.size());
    }

    @Test
    public void getList() {
    }

    @Test
    public void getDatabaseProductName() {
    }

    @Test
    public void get() {
    }

    @Test
    public void save() {
    }
}