package com.bizdk.generator.service;

import com.bizdk.generator.common.service.BaseService;
import com.bizdk.generator.entity.TableColumnEntity;

import java.util.List;

/**
 * 表字段
*/
public interface TableColumnService extends BaseService<TableColumnEntity> {

    List<TableColumnEntity> getByTableId(Long tableId);

    void deleteBatchTableIds(Long[] tableIds);

    /**
     * 修改表字段数据
     *
     * @param tableId        表ID
     * @param tableFieldList 字段列表
     */
    void updateTableColumn(Long tableId, List<TableColumnEntity> tableFieldList);

    /**
     * 初始化字段数据
     * @param tableColumnList
     */
    void initColumnList(List<TableColumnEntity> tableColumnList);
}