package com.bizdk.generator.service;

import com.bizdk.generator.config.template.GeneratorInfo;

import java.util.zip.ZipOutputStream;

/**
 * 代码生成
*/
public interface GeneratorService {

    void downloadCode(GeneratorInfo generatorInfo, ZipOutputStream zip);

    void generatorCode(GeneratorInfo generatorInfo);
}
