package com.bizdk.generator.service;

import com.bizdk.generator.common.page.PageResult;
import com.bizdk.generator.common.query.Query;
import com.bizdk.generator.common.service.BaseService;
import com.bizdk.generator.entity.BaseClassEntity;

import java.util.List;

/**
 * 基类管理
*/
public interface BaseClassService extends BaseService<BaseClassEntity> {

    PageResult<BaseClassEntity> page(Query query);

    List<BaseClassEntity> getList();
}