package com.bizdk.generator.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bizdk.generator.common.page.PageResult;
import com.bizdk.generator.common.query.Query;
import com.bizdk.generator.common.service.impl.BaseServiceImpl;
import com.bizdk.generator.mapper.BaseClassMapper;
import com.bizdk.generator.entity.BaseClassEntity;
import com.bizdk.generator.service.BaseClassService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 基类管理
*/
@Service("BaseClassServiceImpl")
public class BaseClassServiceImpl extends BaseServiceImpl<BaseClassMapper, BaseClassEntity> implements BaseClassService {

    @Override
    public PageResult<BaseClassEntity> page(Query query) {
        IPage<BaseClassEntity> page = baseMapper.selectPage(
                getPage(query), getWrapper(query)
        );

        return new PageResult<>(page.getRecords(), page.getTotal());
    }

    @Override
    public List<BaseClassEntity> getList() {
        return baseMapper.selectList(null);
    }

    @Override
    public boolean save(BaseClassEntity entity) {
        entity.setCreateTime(new Date());
        return super.save(entity);
    }
}