package com.bizdk.generator.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bizdk.generator.common.page.PageResult;
import com.bizdk.generator.common.query.Query;
import com.bizdk.generator.common.service.impl.BaseServiceImpl;
import com.bizdk.generator.mapper.TypeMappingMapper;
import com.bizdk.generator.service.TypeMappingService;
import com.bizdk.generator.entity.TypeMappingEntity;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.*;
import java.util.stream.Collectors;


/**
 * 字段类型管理
*/
@Service
public class TypeMappingServiceImpl extends BaseServiceImpl<TypeMappingMapper, TypeMappingEntity> implements TypeMappingService {

    @Override
    public PageResult<TypeMappingEntity> page(Query query) {
        IPage<TypeMappingEntity> page = baseMapper.selectPage(
                getPage(query),
                getWrapper(query)
        );
        return new PageResult<>(page.getRecords(), page.getTotal());
    }

    @Override
    public Map<String, TypeMappingEntity> getMap() {
        List<TypeMappingEntity> list = baseMapper.selectList(null);
        Map<String, TypeMappingEntity> map = new LinkedHashMap<>(list.size());
        for (TypeMappingEntity entity : list) {
            map.put(entity.getColumnType().toLowerCase(), entity);
        }
        return map;
    }

    @Override
    public Set<String> getPackageByTableId(Long tableId) {
        Set<String> importList = baseMapper.getPackageByTableId(tableId);

        return importList.stream().filter(StrUtil::isNotBlank).collect(Collectors.toSet());
    }

    @Override
    public Set<String> listAttrType() {
        return baseMapper.listAttrType();
    }

    @Override
    public boolean save(TypeMappingEntity entity) {
        entity.setCreateTime(new Date());
        boolean saveFlag = false;
        try {
            saveFlag = super.save(entity);
        } catch (DuplicateKeyException dkex) {
            dkex.printStackTrace();
        }
        return saveFlag;
    }
}