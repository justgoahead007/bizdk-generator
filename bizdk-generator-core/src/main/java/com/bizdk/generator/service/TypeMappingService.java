package com.bizdk.generator.service;

import com.bizdk.generator.common.page.PageResult;
import com.bizdk.generator.common.query.Query;
import com.bizdk.generator.common.service.BaseService;
import com.bizdk.generator.entity.TypeMappingEntity;

import java.util.Map;
import java.util.Set;

/**
 * 字段类型管理
*/
public interface TypeMappingService extends BaseService<TypeMappingEntity> {
    PageResult<TypeMappingEntity> page(Query query);

    Map<String, TypeMappingEntity> getMap();

    /**
     * 根据tableId，获取包列表
     *
     * @param tableId 表ID
     * @return 返回包列表
     */
    Set<String> getPackageByTableId(Long tableId);

    Set<String> listAttrType();
}