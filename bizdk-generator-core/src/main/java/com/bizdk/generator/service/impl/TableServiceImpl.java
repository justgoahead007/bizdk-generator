package com.bizdk.generator.service.impl;

import cn.hutool.core.text.NamingCase;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.bizdk.generator.common.exception.ServerException;
import com.bizdk.generator.common.page.PageResult;
import com.bizdk.generator.common.query.Query;
import com.bizdk.generator.common.service.impl.BaseServiceImpl;
import com.bizdk.generator.config.GenDataSource;
import com.bizdk.generator.enums.FormLayoutEnum;
import com.bizdk.generator.enums.GeneratorTypeEnum;
import com.bizdk.generator.mapper.TableMapper;
import com.bizdk.generator.utils.GenUtils;
import lombok.AllArgsConstructor;
import com.bizdk.generator.config.template.TemplateUtil;
import com.bizdk.generator.entity.TableEntity;
import com.bizdk.generator.entity.TableColumnEntity;
import com.bizdk.generator.service.DataSourceService;
import com.bizdk.generator.service.TableColumnService;
import com.bizdk.generator.service.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * 数据表
*/
@Service
@AllArgsConstructor
public class TableServiceImpl extends BaseServiceImpl<TableMapper, TableEntity> implements TableService {
    @Autowired
    private TableColumnService columnService;

    @Autowired
    private DataSourceService dataSourceService;

    @Autowired
    private TemplateUtil generatorConfig;

    @Override
    public PageResult<TableEntity> page(Query query) {
        IPage<TableEntity> page = baseMapper.selectPage(
                getPage(query),
                getWrapper(query)
        );
        return new PageResult<>(page.getRecords(), page.getTotal());
    }

    @Override
    public TableEntity getByTableName(String tableName) {
        LambdaQueryWrapper<TableEntity> queryWrapper = Wrappers.lambdaQuery();
        return baseMapper.selectOne(queryWrapper.eq(TableEntity::getTableName, tableName));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteBatchIds(Long[] ids) {
        // 删除表
        baseMapper.deleteBatchIds(Arrays.asList(ids));

        // 删除列
        columnService.deleteBatchTableIds(ids);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void tableImport(Long datasourceId, String tableName) {
        GenDataSource dataSource = null;
        try {
            // 初始化配置信息
            dataSource = dataSourceService.get(datasourceId);
            // 查询表是否存在
            TableEntity table = this.getByTableName(tableName);
            // 表存在
            if (table != null) {
                throw new ServerException(tableName + "已存在");
            }

            // 从数据库获取表信息
            table = GenUtils.getTable(dataSource, tableName);

            // 保存表信息
            table.setFormLayout(FormLayoutEnum.ONE.getValue());
            table.setBeanName(NamingCase.toPascalCase(tableName));
            table.setFunctionName(GenUtils.getFunctionName(tableName));
            table.setCreateTime(new Date());
            this.save(table);

            // 获取原生字段数据
            List<TableColumnEntity> columnEntityList = GenUtils.getTableColumnList(dataSource, table.getId(), table.getTableName());
            // 初始化字段数据
            columnService.initColumnList(columnEntityList);

            // 保存列数据
            columnEntityList.forEach(columnService::save);
        } finally {
            if (null != dataSource && null != dataSource.getConnection()) {
                try {
                    //释放数据源
                    dataSource.getConnection().close();
                } catch (SQLException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void sync(Long id) {
        TableEntity table = this.getById(id);

        GenDataSource dataSource = null;
        try {
            // 初始化配置信息
            dataSource = dataSourceService.get(table.getDatasourceId());

            // 从数据库获取表字段列表
            List<TableColumnEntity> columnEntities = GenUtils.getTableColumnList(dataSource, table.getId(), table.getTableName());
            if (columnEntities.size() == 0) {
                throw new ServerException("同步失败，请检查数据库表：" + table.getTableName());
            }

            List<String> dbTableColumnNameList = columnEntities.stream().map(TableColumnEntity::getColumnName).collect(Collectors.toList());

            // 表字段列表
            List<TableColumnEntity> columnEntityList = columnService.getByTableId(id);

            Map<String, TableColumnEntity> tableFieldMap = columnEntityList.stream().collect(Collectors.toMap(TableColumnEntity::getColumnName, Function.identity()));

            // 初始化字段数据
            columnService.initColumnList(columnEntities);

            // 同步表结构字段
            columnEntities.forEach(column -> {
                // 新增字段
                if (!tableFieldMap.containsKey(column.getColumnName())) {
                    columnService.save(column);
                    return;
                }

                // 修改字段
                TableColumnEntity columnEntity = tableFieldMap.get(column.getColumnName());
                columnEntity.setPrimaryPk(column.isPrimaryPk());
                columnEntity.setColumnComment(column.getColumnComment());
                columnEntity.setColumnType(column.getColumnType());
                columnEntity.setAttrType(column.getAttrType());

                columnService.updateById(columnEntity);
            });

            // 删除数据库表中没有的字段
            List<TableColumnEntity> delFieldList = columnEntityList.stream().filter(field -> !dbTableColumnNameList.contains(field.getColumnName())).collect(Collectors.toList());
            if (delFieldList.size() > 0) {
                List<Long> fieldIds = delFieldList.stream().map(TableColumnEntity::getId).collect(Collectors.toList());
                columnService.removeBatchByIds(fieldIds);
            }
        } finally {
            if (null != dataSource && null != dataSource.getConnection()) {
                try {
                    //释放数据源
                    dataSource.getConnection().close();
                } catch (SQLException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    @Override
    public List<TableEntity> tableList(Long datasourceId) {
        GenDataSource dataSource = null;
        // 初始化配置信息
        dataSource = dataSourceService.get(datasourceId);
        GenUtils.getTableList(dataSource);

        return null;
    }
}