package com.bizdk.generator.service.impl;

import cn.hutool.core.text.NamingCase;
import com.bizdk.generator.common.service.impl.BaseServiceImpl;
import com.bizdk.generator.enums.AutoFillEnum;
import com.bizdk.generator.mapper.TableColumnMapper;
import com.bizdk.generator.service.TypeMappingService;
import com.bizdk.generator.service.TableColumnService;
import lombok.AllArgsConstructor;
import com.bizdk.generator.entity.TypeMappingEntity;
import com.bizdk.generator.entity.TableColumnEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 表字段
*/
@Service
@AllArgsConstructor
public class TableColumnServiceImpl extends BaseServiceImpl<TableColumnMapper, TableColumnEntity> implements TableColumnService {
    @Autowired
    private TypeMappingService fieldTypeService;

    @Override
    public List<TableColumnEntity> getByTableId(Long tableId) {
        return baseMapper.getByTableId(tableId);
    }

    @Override
    public void deleteBatchTableIds(Long[] tableIds) {
        baseMapper.deleteBatchTableIds(tableIds);
    }

    @Override
    public void updateTableColumn(Long tableId, List<TableColumnEntity> columnEntities) {
        // 更新字段数据
        int sort = 0;
        for (TableColumnEntity tableField : columnEntities) {
            tableField.setSort(sort++);
            this.updateById(tableField);
        }
    }

    @Override
    public void initColumnList(List<TableColumnEntity> tableColumnList) {
        // 字段类型、属性类型映射
        Map<String, TypeMappingEntity> fieldTypeMap = fieldTypeService.getMap();
        int index = 0;
        for (TableColumnEntity columnEntity : tableColumnList) {
            columnEntity.setAttrName(NamingCase.toCamelCase(columnEntity.getColumnName()));
            // 获取字段对应的类型
            TypeMappingEntity fieldTypeMapping = fieldTypeMap.get(columnEntity.getColumnType().toLowerCase());
            if (fieldTypeMapping == null) {
                // 没找到对应的类型，则为Object类型
                columnEntity.setAttrType("Object");
            } else {
                columnEntity.setAttrType(fieldTypeMapping.getAttrType());
                columnEntity.setPackageName(fieldTypeMapping.getPackageName());
            }

            columnEntity.setAutoFill(AutoFillEnum.DEFAULT.name());
            columnEntity.setFormItem(true);
            columnEntity.setGridItem(true);
            columnEntity.setQueryType("=");
            columnEntity.setQueryFormType("text");
            columnEntity.setFormType("text");
            columnEntity.setSort(index++);
        }
    }

}