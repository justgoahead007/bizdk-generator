package com.bizdk.generator.service;

import com.bizdk.generator.common.page.PageResult;
import com.bizdk.generator.common.query.Query;
import com.bizdk.generator.common.service.BaseService;
import com.bizdk.generator.entity.TemplateEntity;

import java.io.IOException;

/**
 * 项目名变更
*/
public interface TemplateService extends BaseService<TemplateEntity> {

    PageResult<TemplateEntity> page(Query query);

    byte[] download(TemplateEntity project) throws IOException;

}