package com.bizdk.generator.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.bizdk.generator.common.exception.ServerException;
import com.bizdk.generator.common.utils.DateUtils;
import com.bizdk.generator.config.DataModel;
import com.bizdk.generator.config.DataModelConstant;
import com.bizdk.generator.config.template.*;
import com.bizdk.generator.service.*;
import com.bizdk.generator.utils.TemplateUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.bizdk.generator.entity.BaseClassEntity;
import com.bizdk.generator.entity.TableEntity;
import com.bizdk.generator.entity.TableColumnEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * 代码生成
*/
@Service
@Slf4j
@AllArgsConstructor
public class GeneratorServiceImpl implements GeneratorService {
    @Autowired
    private DataSourceService datasourceService;
    @Autowired
    private TypeMappingService typeMappingService;
    @Autowired
    private BaseClassService baseClassService;
    @Autowired
    private TemplateUtil templateUtil;
    @Autowired
    private TableService tableService;
    @Autowired
    private TableColumnService tableColumnService;

    @Override
    public void downloadCode(GeneratorInfo generatorInfo, ZipOutputStream zip) {
        for (Long tableId : generatorInfo.getTableIds()) {
            // 代码生成信息
            TemplateConfig templateConfig = templateUtil.getTemplateConfig(generatorInfo.getTemplatePath());
            // 数据模型
            DataModel dataModel = getDataModel(generatorInfo, tableId);

            // 渲染模板并输出
            log.info("生成zip文件 目录如下：");
            for (TemplateInfo template : templateConfig.getTemplates()) {
                dataModel.put(DataModelConstant.TEMPLATE_NAME, template.getTemplateName());
                String content = TemplateUtils.getContent(template.getTemplateContent(), dataModel);
                String path = TemplateUtils.getContent(template.getGeneratorPath(), dataModel);
                log.info(path);
                try {
                    // 添加到zip
                    zip.putNextEntry(new ZipEntry(path));
                    IoUtil.writeUtf8(zip, false, content);
                    zip.flush();
                    zip.closeEntry();
                } catch (IOException e) {
                    throw new ServerException("模板写入失败：" + path, e);
                }
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void generatorCode(GeneratorInfo generatorInfo) {
        if (StrUtil.isBlank(generatorInfo.getCodePath())) {
            String wdir = System.getProperty("java.io.tmpdir");
            generatorInfo.setCodePath(wdir + "demo");
        }

        for (Long tableId : generatorInfo.getTableIds()) {
            // 代码生成信息
            TemplateConfig templateConfig = templateUtil.getTemplateConfig(generatorInfo.getTemplatePath());
            // 数据模型
            DataModel dataModel = getDataModel(generatorInfo, tableId);

            // 渲染模板并输出
            for (TemplateInfo template : templateConfig.getTemplates()) {
                dataModel.put(DataModelConstant.TEMPLATE_NAME, template.getTemplateName());
                String content = TemplateUtils.getContent(template.getTemplateContent(), dataModel);
                String path = TemplateUtils.getContent(template.getGeneratorPath(), dataModel);
                log.info("生成文件 {}", path);
                try {
                    FileUtil.writeUtf8String(content, path);
                } catch (Exception ex) {
                    log.error("解析模板文件 " + template.getTemplateName() + "失败", ex);
                }
            }
        }
    }

    private DataModel getDataModel(GeneratorInfo generatorInfo, Long tableId) {
        // 数据模型
        DataModel dataModel = getDataModel(tableId);


        dataModel.put(DataModelConstant.AUTHOR, generatorInfo.getAuthor());
        dataModel.put(DataModelConstant.EMAIL, generatorInfo.getEmail());
        dataModel.put(DataModelConstant.VERSION, generatorInfo.getVersion());
        // 生成路径
        dataModel.put(DataModelConstant.CODE_PATH, generatorInfo.getCodePath());

        // 开发者信息
        setAuthorInfo(dataModel, generatorInfo);

        // 项目信息
        setProjectInfo(dataModel, generatorInfo);
        return dataModel;
    }

    /**
     * 获取渲染的数据模型
     *
     * @param tableId 表ID
     */
    private DataModel getDataModel(Long tableId) {
        // 表信息
        TableEntity table = tableService.getById(tableId);
        List<TableColumnEntity> columnEntities = tableColumnService.getByTableId(tableId);
        table.setColumnEntities(columnEntities);

        // 数据模型
        DataModel dataModel = new DataModel();

        // 获取数据库类型
        setDbType(dataModel, table);

        // 设置基类信息
        setBaseClass(dataModel, table);

        // 导入的包列表
        setImportList(dataModel, table);

        // 表信息
        setTableInfo(dataModel, table);

        // 设置字段信息
        setFieldTypeList(dataModel, table);

        return dataModel;
    }

    private void setTableInfo(DataModel dataModel, TableEntity table) {
        dataModel.put(DataModelConstant.TABLE_NAME, table.getTableName());
        dataModel.put(DataModelConstant.TABLE_COMMENT, table.getTableComment());
        dataModel.put(DataModelConstant.BEAN_NAME, table.getBeanName());
        dataModel.put(DataModelConstant.BEAN_NAME_LF, StrUtil.lowerFirst(table.getBeanName()));
        dataModel.put(DataModelConstant.COLUMN_LIST, table.getColumnEntities());

        dataModel.put(DataModelConstant.FUNCTION_NAME, table.getFunctionName());
        dataModel.put(DataModelConstant.FUNCTION_NAME_UF, StrUtil.upperFirst(table.getFunctionName()));
        dataModel.put(DataModelConstant.FORM_LAYOUT, table.getFormLayout());
    }

    private void setImportList(DataModel dataModel, TableEntity table) {
        Set<String> importList = typeMappingService.getPackageByTableId(table.getId());
        dataModel.put(DataModelConstant.IMPORT_LIST, importList);
    }

    private void setDbType(DataModel dataModel, TableEntity table) {
        String dbType = datasourceService.getDatabaseProductName(table.getDatasourceId());
        dataModel.put(DataModelConstant.DB_TYPE, dbType);
    }

    private void setProjectInfo(DataModel dataModel, GeneratorInfo generatorInfo) {
        dataModel.put(DataModelConstant.PACKAGE, generatorInfo.getPackageName());
        dataModel.put(DataModelConstant.PACKAGE_PATH, generatorInfo.getPackageName().replace(".", "/"));
        dataModel.put(DataModelConstant.VERSION, generatorInfo.getVersion());
        dataModel.put(DataModelConstant.MODULE_NAME, generatorInfo.getModuleName());
        dataModel.put(DataModelConstant.MODULE_NAME_UF, StrUtil.upperFirst(generatorInfo.getModuleName()));
    }

    private void setAuthorInfo(DataModel dataModel, GeneratorInfo generatorInfo) {
        dataModel.put(DataModelConstant.AUTHOR, generatorInfo.getAuthor());
        dataModel.put(DataModelConstant.EMAIL, generatorInfo.getEmail());
        dataModel.put(DataModelConstant.DATETIME, DateUtils.format(new Date(), DateUtils.DATE_TIME_PATTERN));
        dataModel.put(DataModelConstant.DATE, DateUtils.format(new Date(), DateUtils.DATE_PATTERN));
    }

    /**
     * 设置基类信息
     *
     * @param dataModel 数据模型
     * @param table     表
     */
    private void setBaseClass(DataModel dataModel, TableEntity table) {
        if (table.getBaseclassId() == null) {
            return;
        }

        // 基类
        BaseClassEntity baseClass = baseClassService.getById(table.getBaseclassId());
        baseClass.setPackageName(baseClass.getPackageName());
        dataModel.put(DataModelConstant.BASE_CLASS, baseClass);

        // 基类字段
        String[] attrs = baseClass.getAttrs().split(",");

        // 标注为基类字段
        for (TableColumnEntity columnEntity : table.getColumnEntities()) {
            if (ArrayUtil.contains(attrs, columnEntity.getAttrName())) {
                columnEntity.setBaseAttr(true);
            }
        }
    }

    /**
     * 设置字段分类信息
     *
     * @param dataModel 数据模型
     * @param table     表
     */
    private void setFieldTypeList(DataModel dataModel, TableEntity table) {
        List<TableColumnEntity> columnEntities = tableColumnService.getByTableId(table.getId());
        table.setColumnEntities(columnEntities);

        // 主键列表 (支持多主键)
        List<TableColumnEntity> primaryList = new ArrayList<>();
        // 表单列表
        List<TableColumnEntity> formList = new ArrayList<>();
        // 网格列表
        List<TableColumnEntity> gridList = new ArrayList<>();
        // 查询列表
        List<TableColumnEntity> queryList = new ArrayList<>();

        for (TableColumnEntity columnEntity : table.getColumnEntities()) {
            if (columnEntity.isPrimaryPk()) {
                primaryList.add(columnEntity);
            }
            if (columnEntity.isFormItem()) {
                formList.add(columnEntity);
            }
            if (columnEntity.isGridItem()) {
                gridList.add(columnEntity);
            }
            if (columnEntity.isQueryItem()) {
                queryList.add(columnEntity);
            }
        }
        dataModel.put(DataModelConstant.PRIMARY_LIST, primaryList);
        dataModel.put(DataModelConstant.FORM_LIST, formList);
        dataModel.put(DataModelConstant.GRID_LIST, gridList);
        dataModel.put(DataModelConstant.QUERY_LIST, queryList);
    }

}