package com.bizdk.generator.config.template;

import lombok.Data;

import java.util.List;

/**
 * 代码生成信息
*/
@Data
public class GeneratorInfo {
    /**
     * 作者
     */
    private String author;
    /**
     * 邮箱
     */
    private String email;

    /**
     *
     */
    private List<Long> tableIds;

    /**
     * 模板路径
     */
    private String templatePath;

    /**
     * 项目包名
     */
    private String packageName;

    /**
     * 模块名称
     */
    private String moduleName;

    /**
     * 项目版本号
     */
    private String version;
    /**
     * 生成方式  1：zip压缩包   2：自定义目录
     */
    private Integer generatorType;
    /**
     * 代码生成路径
     */
    private String codePath;
}
