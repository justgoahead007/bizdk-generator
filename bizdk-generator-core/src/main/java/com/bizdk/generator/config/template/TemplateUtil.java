package com.bizdk.generator.config.template;

import cn.hutool.core.util.StrUtil;
import com.bizdk.generator.common.exception.ServerException;
import com.bizdk.generator.common.utils.JacksonUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * 代码生成配置内容
 */
@Component
public class TemplateUtil {

    public TemplateConfig getTemplateConfig(String templatePath) {
        // 模板路径，如果不是以/结尾，则添加/
        if (!StrUtil.endWith(templatePath, '/')) {
            templatePath = templatePath + "/";
        }

        String wdir = System.getProperty("user.dir");
        StringBuilder strBui = new StringBuilder();
        strBui.append(wdir).append("/template/").append(templatePath);

        String path = strBui.toString().replaceAll("\\\\", "\\/");

        File configFile = new File(path + "config.json");

        // 模板配置文件
        if (configFile == null) {
            throw new ServerException("模板配置文件，config.json不存在");
        }

        String configContent = "";

        try (FileInputStream configIn = new FileInputStream(configFile);) {
            // 读取模板配置文件
            configContent = StreamUtils.copyToString(configIn, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new ServerException("读取config.json配置文件失败");
        }

        TemplateConfig templateConfig = JacksonUtils.parseObject(configContent, TemplateConfig.class);
        for (TemplateInfo templateInfo : templateConfig.getTemplates()) {
            // 模板文件
            File templateFile = new File(path + templateInfo.getTemplateName());
            if (templateFile == null || !templateFile.exists()) {
                throw new ServerException("模板文件 " + templateInfo.getTemplateName() + " 不存在");
            }
            try (InputStream templateIn = new FileInputStream(templateFile);) {
                // 读取模板内容
                String templateContent = StreamUtils.copyToString(templateIn, StandardCharsets.UTF_8);
                templateInfo.setTemplateContent(templateContent);
            } catch (IOException e) {
                throw new ServerException("读取" + templateInfo.getTemplateName() + "文件失败");
            }
        }
        return templateConfig;
    }
}
