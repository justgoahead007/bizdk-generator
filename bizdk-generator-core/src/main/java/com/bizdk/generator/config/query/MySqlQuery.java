package com.bizdk.generator.config.query;

import cn.hutool.core.util.StrUtil;
import com.bizdk.generator.config.DbType;

import java.util.StringJoiner;

/**
 * MySQL查询
*/
public class MySqlQuery implements DialectQuery {

    @Override
    public DbType dbType() {
        return DbType.MySQL;
    }

    @Override
    public String tableSql(String tableName) {
        StringBuilder sql = new StringBuilder();
        sql.append("select ")
                .append("table_name, ")
                .append("table_comment ")
                .append("from information_schema.tables ")
                .append("where table_schema = (select database()) ");
        // 表名查询
        if (StrUtil.isNotBlank(tableName)) {
            sql.append("and table_name = '").append(tableName).append("' ");
        }
        sql.append("order by table_name asc");

        return sql.toString();
    }

    @Override
    public String tableName() {
        return "table_name";
    }

    @Override
    public String tableComment() {
        return "table_comment";
    }

    @Override
    public String tableColumnsSql() {
        StringJoiner sql = new StringJoiner(" \n");
        sql.add("select ")
                .add("column_name, ")
                .add("data_type, ")
                .add("column_comment, ")
                .add("column_key ")
                .add("from information_schema.columns ")
                .add("where table_name = '%s' and table_schema = (select database()) order by ordinal_position");
        return sql.toString();
    }

    @Override
    public String columnName() {
        return "column_name";
    }

    @Override
    public String columnType() {
        return "data_type";
    }

    @Override
    public String columnComment() {
        return "column_comment";
    }

    @Override
    public String columnKey() {
        return "column_key";
    }
}
