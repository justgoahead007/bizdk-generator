package com.bizdk.generator.config;

public interface DataModelConstant {
    String TABLE_NAME = "tableName";
    String TABLE_COMMENT = "tableComment";
    String BEAN_NAME = "BeanName";
    String BEAN_NAME_LF = "beanName";
    String COLUMN_LIST = "columnList";
    String PRIMARY_LIST = "primaryList";
    String FORM_LIST = "formList";
    String GRID_LIST = "gridList";
    String QUERY_LIST = "queryList";

    String IMPORT_LIST = "importList";
    String DB_TYPE = "dbType";
    String PACKAGE = "package";
    String PACKAGE_PATH = "packagePath";
    String VERSION = "version";
    String MODULE_NAME = "moduleName";
    String MODULE_NAME_UF = "ModuleName";
    String FUNCTION_NAME = "functionName";
    String FUNCTION_NAME_UF = "FunctionName";
    String FORM_LAYOUT = "formLayout";

    String AUTHOR = "author";
    String EMAIL = "email";
    String DATETIME = "datetime";
    String DATE = "date";

    String BASE_CLASS = "baseClass";
    String CODE_PATH = "codePath";
    String TEMPLATE_NAME = "templateName";
}
