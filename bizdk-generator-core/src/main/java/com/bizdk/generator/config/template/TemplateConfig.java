package com.bizdk.generator.config.template;

import lombok.Data;

import java.util.List;

@Data
public class TemplateConfig {

    List<TemplateInfo> templates;
}
