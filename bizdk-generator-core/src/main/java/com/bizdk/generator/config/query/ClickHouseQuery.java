
package com.bizdk.generator.config.query;

import cn.hutool.core.util.StrUtil;
import com.bizdk.generator.config.DbType;

/**
 * ClickHouse 表数据查询
 *
 * @author ratelfu
 * @since 2021-03-10
 */
public class ClickHouseQuery implements DialectQuery {


    @Override
    public String tableColumnsSql() {
        return "select * from system.columns where table='%s'";
    }

    @Override
    public DbType dbType() {
        return DbType.Clickhouse;
    }

    @Override
    public String tableSql(String tableName) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM system.tables WHERE 1=1 ");

        // 表名查询
        if (StrUtil.isNotBlank(tableName)) {
            sql.append("and name = '").append(tableName).append("' ");
        }
        return sql.toString();
    }

    @Override
    public String tableName() {
        return "name";
    }

    @Override
    public String tableComment() {
        return "comment";
    }

    @Override
    public String columnName() {
        return "name";
    }


    @Override
    public String columnType() {
        return "type";
    }


    @Override
    public String columnComment() {
        return "comment";
    }


    @Override
    public String columnKey() {
        return "is_in_primary_key";
    }



}
