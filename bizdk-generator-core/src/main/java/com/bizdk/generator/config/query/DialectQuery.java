package com.bizdk.generator.config.query;


import com.bizdk.generator.config.DbType;

/**
 * Query
*/
public interface DialectQuery {

    /**
     * 数据库类型
     */
    DbType dbType();

    /**
     * 表信息查询 SQL
     */
    String tableSql(String tableName);

    /**
     * 表名称
     */
    String tableName();

    /**
     * 表注释
     */
    String tableComment();

    /**
     * 表字段信息查询 SQL
     */
    String tableColumnsSql();

    /**
     * 字段名称
     */
    String columnName();

    /**
     * 字段类型
     */
    String columnType();

    /**
     * 字段注释
     */
    String columnComment();

    /**
     * 主键字段
     */
    String columnKey();
}
