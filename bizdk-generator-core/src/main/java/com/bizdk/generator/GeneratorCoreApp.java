package com.bizdk.generator;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.StringJoiner;

@MapperScan(basePackages = {"com.bizdk.generator.mapper"})
@SpringBootApplication
public class GeneratorCoreApp {

    public static void main(String[] args) {
        SpringApplication.run(GeneratorCoreApp.class, args);
        StringJoiner strJoi = new StringJoiner("\n");
        strJoi.add("=========================启动完成！！！===============================");
        System.out.println(strJoi.toString());
    }
}
