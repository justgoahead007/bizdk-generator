package com.bizdk.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bizdk.generator.entity.TemplateEntity;

/**
 * 项目名变更
*/
public interface TemplateMapper extends BaseMapper<TemplateEntity> {

}