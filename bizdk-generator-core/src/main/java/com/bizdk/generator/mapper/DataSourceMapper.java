package com.bizdk.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bizdk.generator.entity.DataSourceEntity;

/**
 * 数据源管理
*/
public interface DataSourceMapper extends BaseMapper<DataSourceEntity> {

}