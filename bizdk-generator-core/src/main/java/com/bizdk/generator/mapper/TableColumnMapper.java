package com.bizdk.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bizdk.generator.entity.TableColumnEntity;

import java.util.List;

/**
 * 表字段
*/
public interface TableColumnMapper extends BaseMapper<TableColumnEntity> {

    List<TableColumnEntity> getByTableId(Long tableId);

    void deleteBatchTableIds(Long[] tableIds);
}
