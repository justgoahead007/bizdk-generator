package com.bizdk.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bizdk.generator.entity.BaseClassEntity;

/**
 * 基类管理
*/
public interface BaseClassMapper extends BaseMapper<BaseClassEntity> {

}