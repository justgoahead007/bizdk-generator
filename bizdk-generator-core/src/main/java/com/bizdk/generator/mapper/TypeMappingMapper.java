package com.bizdk.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bizdk.generator.entity.TypeMappingEntity;

import java.util.Set;

/**
 * 字段类型管理
*/
public interface TypeMappingMapper extends BaseMapper<TypeMappingEntity> {

    /**
     * 根据tableId，获取包列表
     */
    Set<String> getPackageByTableId(Long tableId);

    /**
     * 获取全部字段类型
     */
    Set<String> listAttrType();
}