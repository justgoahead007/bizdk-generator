package com.bizdk.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bizdk.generator.entity.TableEntity;

/**
 * 数据表
*/
public interface TableMapper extends BaseMapper<TableEntity> {

}
