package com.bizdk.generator.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 数据表
*/
@Data
@TableName("bd_table")
public class TableEntity {
    @TableId
    private Long id;
    /**
     * 表名
     */
    private String tableName;
    /**
     * bean名称
     */
    private String beanName;
    /**
     * 表备注
     */
    private String tableComment;

    /**
     * 功能名
     */
    private String functionName;

    /**
     * 表单布局  1：一列   2：两列
     */
    private Integer formLayout;

    /**
     * 数据源ID
     */
    private Long datasourceId;
    /**
     * 基类ID
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Long baseclassId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 字段列表
     */
    @TableField(exist = false)
    private List<TableColumnEntity> columnEntities;
}
