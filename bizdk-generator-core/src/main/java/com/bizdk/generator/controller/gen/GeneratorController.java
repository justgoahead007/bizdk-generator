package com.bizdk.generator.controller.gen;

import cn.hutool.core.io.IoUtil;
import com.bizdk.generator.common.utils.DateUtils;
import com.bizdk.generator.common.utils.Result;
import com.bizdk.generator.config.template.GeneratorInfo;
import com.bizdk.generator.service.GeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.Data;
import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.Objects;
import java.util.zip.ZipOutputStream;

/**
 * 代码生成
*/
@Controller
@RequestMapping("/gen/generator")
public class GeneratorController {
    @Autowired
    private GeneratorService generatorService;


    /**
     * 生成代码 下载文件
     */
    @PostMapping("/download")
    public void download(@RequestBody GeneratorInfo generatorInfo, HttpServletResponse response) throws Exception {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
             ZipOutputStream zip = new ZipOutputStream(outputStream);) {

            generatorService.downloadCode(generatorInfo, zip);
            // zip压缩包数据
            byte[] data = outputStream.toByteArray();

            response.reset();
            response.setHeader("Content-Disposition", "attachment; filename=code" + DateUtils.format(new Date(), "yyyyMMddHHmmss") + ".zip");
            response.addHeader("Content-Length", "" + data.length);
            response.setContentType("application/octet-stream; charset=UTF-8");
            IoUtil.write(response.getOutputStream(), false, data);
        } catch (Exception ex){
        }
    }

    /**
     * 生成代码（自定义目录）
     */
    @ResponseBody
    @PostMapping("/code")
    public Result<String> code(@RequestBody GeneratorInfo generatorInfo, HttpServletResponse response) throws Exception {
        // 生成代码
        generatorService.generatorCode(generatorInfo);
        return Result.ok();
    }

}