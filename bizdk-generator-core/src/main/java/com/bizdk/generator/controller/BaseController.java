package com.bizdk.generator.controller;

import cn.hutool.core.util.StrUtil;
import com.bizdk.generator.common.page.PageResult;
import com.bizdk.generator.common.query.Query;
import com.bizdk.generator.common.utils.Result;
import com.bizdk.generator.entity.BaseClassEntity;
import com.bizdk.generator.service.BaseClassService;
import com.bizdk.generator.utils.ServletUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;

/**
 * 基类管理
*/
public abstract class BaseController {

    /**
     * 获取request
     */
    public HttpServletRequest getRequest()
    {
        return ServletUtils.getRequest();
    }

    /**
     * 获取response
     */
    public HttpServletResponse getResponse()
    {
        return ServletUtils.getResponse();
    }

    /**
     * 获取session
     */
    public HttpSession getSession()
    {
        return getRequest().getSession();
    }

    /**
     * 页面跳转
     */
    public String redirect(String url)
    {
        return StrUtil.format("redirect:{}", url);
    }


    protected abstract String getPrefix();

    @GetMapping("/list")
    public Object list(Query query) {
        return getPrefix() + "/list";
    }

    @GetMapping("/add")
    public Object add() {
        return getPrefix() + "/add";
    }

    @GetMapping("/edit/{id}")
    public Object edit(@PathVariable("id") Long id) {
        return getPrefix() + "/edit";
    }
}