package com.bizdk.generator.controller.monitor;

import com.bizdk.generator.controller.BaseController;
import com.bizdk.generator.domain.Server;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 服务器监控
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/monitor/server")
public class ServerController extends BaseController {

    @Override
    protected String getPrefix() {
        return "monitor/server";
    }

    @GetMapping()
    public String server(ModelMap mmap) throws Exception {
        Server server = new Server();
        server.copyTo();
        mmap.put("server", server);
        return getPrefix() + "/server";
    }
}
