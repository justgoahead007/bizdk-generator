package com.bizdk.generator.controller.gen;

import cn.hutool.core.util.BooleanUtil;
import com.bizdk.generator.common.page.PageResult;
import com.bizdk.generator.common.query.Query;
import com.bizdk.generator.common.utils.Result;
import com.bizdk.generator.config.GenDataSource;
import com.bizdk.generator.controller.BaseController;
import com.bizdk.generator.utils.DbUtils;
import com.bizdk.generator.utils.GenUtils;
import lombok.extern.slf4j.Slf4j;
import com.bizdk.generator.entity.DataSourceEntity;
import com.bizdk.generator.entity.TableEntity;
import com.bizdk.generator.service.DataSourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 数据源管理
*/
@Slf4j
@Controller
@RequestMapping("/gen/datasource")
public class DataSourceController extends BaseController {

    @Autowired
    private DataSourceService datasourceService;

    @Override
    protected String getPrefix() {
        return "gen/datasource";
    }

    @ResponseBody
    @RequestMapping("/page")
    public Result<PageResult<DataSourceEntity>> page(Query query) {
        PageResult<DataSourceEntity> page = datasourceService.page(query);

        return Result.ok(page);
    }


    @Override
    @GetMapping("/edit/{id}")
    public Object edit(@PathVariable("id")Long id) {
        DataSourceEntity data = datasourceService.getById(id);
        ModelAndView modelAndView = new ModelAndView(super.edit(id).toString());
        modelAndView.addObject("dataSource", data);
        return modelAndView;
    }

    @ResponseBody
    @PostMapping("/save")
    public Result<String> save(DataSourceEntity entity) {
        boolean flag = datasourceService.save(entity);
        return BooleanUtil.isTrue(flag)? Result.ok() : Result.error();
    }

    @ResponseBody
    @PutMapping("/update")
    public Result<String> update(DataSourceEntity entity) {
        datasourceService.updateById(entity);

        return Result.ok();
    }

    @ResponseBody
    @PostMapping("/delete")
    public Result<String> delete(Long[] ids) {
        datasourceService.removeBatchByIds(Arrays.asList(ids));

        return Result.ok();
    }


    @GetMapping("/createTable")
    public Object createTable(Query query) {
        return getPrefix() + "/createTable";
    }

    @GetMapping("/importTable")
    public Object importTable(Query query) {
        List<DataSourceEntity> list = datasourceService.list();
        ModelAndView modelAndView = new ModelAndView(getPrefix() + "/importTable");
        modelAndView.addObject("dataSourceList", list);
        return modelAndView;
    }

    /**
     * 根据数据源ID，获取全部数据表
     *
     * @param id 数据源ID
     */
    @ResponseBody
    @PostMapping("/table/list/{id}")
    public Result<List<TableEntity>> tableList(@PathVariable("id") Long id) {
        if (id < 1) {
            return Result.ok(new ArrayList<>());
        }
        try {
            // 获取数据源
            GenDataSource datasource = datasourceService.get(id);
            // 根据数据源，获取全部数据表
            List<TableEntity> tableList = GenUtils.getTableList(datasource);

            return Result.ok(tableList);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("数据源配置错误，请检查数据源配置！");
        }
    }
}