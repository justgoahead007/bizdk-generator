package com.bizdk.generator.controller.gen;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.BooleanUtil;
import com.bizdk.generator.common.page.PageResult;
import com.bizdk.generator.common.query.Query;
import com.bizdk.generator.common.utils.Result;
import com.bizdk.generator.entity.TemplateEntity;
import com.bizdk.generator.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * 项目名变更
*/
@Controller
@RequestMapping("/gen/template")
public class TemplateController {
    @Autowired
    private TemplateService templateService;

    @RequestMapping("/page")
    public Result<PageResult<TemplateEntity>> page(@Valid Query query) {
        PageResult<TemplateEntity> page = templateService.page(query);

        return Result.ok(page);
    }



    @GetMapping("/{id}")
    public Result<TemplateEntity> get(@PathVariable("id") Long id) {
        TemplateEntity entity = templateService.getById(id);

        return Result.ok(entity);
    }

    @PostMapping("/save")
    public Result<String> save(TemplateEntity entity) {
        boolean flag = templateService.save(entity);
        return BooleanUtil.isTrue(flag)? Result.ok() : Result.error();
    }


    @PutMapping("/update")
    public Result<String> update(@Valid TemplateEntity entity) {
        templateService.updateById(entity);

        return Result.ok();
    }

    @PostMapping("/delete")
    public Result<String> delete(List<Long> idList) {
        templateService.removeByIds(idList);

        return Result.ok();
    }

    /**
     * 源码下载
     */
    @GetMapping("/download/{id}")
    public void download(@PathVariable("id") Long id, HttpServletResponse response) throws Exception {
        // 项目信息
        TemplateEntity project = templateService.getById(id);

        byte[] data = templateService.download(project);

        response.reset();
        response.setHeader("Content-Disposition", "attachment; filename=\"" + project.getModifyProjectName() + ".zip\"");
        response.addHeader("Content-Length", "" + data.length);
        response.setContentType("application/octet-stream; charset=UTF-8");

        IoUtil.write(response.getOutputStream(), false, data);
    }
}