package com.bizdk.generator.controller.gen;

import cn.hutool.core.util.BooleanUtil;
import com.bizdk.generator.common.page.PageResult;
import com.bizdk.generator.common.query.Query;
import com.bizdk.generator.common.utils.Result;
import com.bizdk.generator.controller.BaseController;
import com.bizdk.generator.entity.BaseClassEntity;
import com.bizdk.generator.entity.DataSourceEntity;
import com.bizdk.generator.service.BaseClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;

/**
 * 基类管理
*/
@Controller
@RequestMapping("/gen/baseclass")
public class BaseClassController extends BaseController {

    @Autowired
    private BaseClassService baseClassService;

    @Override
    protected String getPrefix() {
        return "gen/baseclass";
    }

    @ResponseBody
    @RequestMapping("/page")
    public Result<PageResult<BaseClassEntity>> page(Query query) {
        PageResult<BaseClassEntity> page = baseClassService.page(query);

        return Result.ok(page);
    }

    @Override
    @GetMapping("/edit/{id}")
    public Object edit(@PathVariable("id")Long id) {
        BaseClassEntity data = baseClassService.getById(id);
        ModelAndView modelAndView = new ModelAndView(super.edit(id).toString());
        modelAndView.addObject("baseClass", data);
        return modelAndView;
    }


    @ResponseBody
    @PostMapping("/save")
    public Result<String> save(BaseClassEntity entity) {
        boolean flag = baseClassService.save(entity);

        return BooleanUtil.isTrue(flag)? Result.ok() : Result.error();
    }

    @ResponseBody
    @PutMapping("/update")
    public Result<String> update(BaseClassEntity entity) {
        baseClassService.updateById(entity);

        return Result.ok();
    }

    @ResponseBody
    @PostMapping("/delete")
    public Result<String> delete(Long[] ids) {
        baseClassService.removeBatchByIds(Arrays.asList(ids));

        return Result.ok();
    }
}