package com.bizdk.generator.controller.monitor;

import com.bizdk.generator.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * druid 监控
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/monitor/data")
public class DruidController extends BaseController {

    @Override
    protected String getPrefix() {
        return "druid";
    }

    @GetMapping()
    public String index() {
        return redirect(getPrefix() + "/index.html");
    }
}
