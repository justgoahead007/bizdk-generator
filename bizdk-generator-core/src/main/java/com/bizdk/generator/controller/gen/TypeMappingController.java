package com.bizdk.generator.controller.gen;

import cn.hutool.core.util.BooleanUtil;
import com.bizdk.generator.common.page.PageResult;
import com.bizdk.generator.common.query.Query;
import com.bizdk.generator.common.utils.Result;
import com.bizdk.generator.controller.BaseController;
import com.bizdk.generator.entity.DataSourceEntity;
import com.bizdk.generator.entity.TypeMappingEntity;
import com.bizdk.generator.service.TypeMappingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * 字段类型管理
*/
@Controller
@RequestMapping("/gen/typemapping")
public class TypeMappingController extends BaseController {

    @Autowired
    private TypeMappingService typeMappingService;

    @Override
    protected String getPrefix() {
        return "gen/typemapping";
    }

    @ResponseBody
    @RequestMapping("/page")
    public Result<PageResult<TypeMappingEntity>> page(Query query) {
        PageResult<TypeMappingEntity> page = typeMappingService.page(query);

        return Result.ok(page);
    }

    @Override
    @GetMapping("/edit/{id}")
    public Object edit(@PathVariable("id") Long id) {
        TypeMappingEntity data = typeMappingService.getById(id);
        ModelAndView modelAndView = new ModelAndView(super.edit(id).toString());
        modelAndView.addObject("typeMapping", data);
        return modelAndView;
    }


    @ResponseBody
    @PostMapping("/save")
    public Result<String> save(TypeMappingEntity entity) {
        boolean flag = typeMappingService.save(entity);
        return BooleanUtil.isTrue(flag)? Result.ok() : Result.error();
    }

    @ResponseBody
    @PutMapping("/update")
    public Result<String> update(TypeMappingEntity entity) {
        typeMappingService.updateById(entity);

        return Result.ok();
    }

    @ResponseBody
    @PostMapping("/delete")
    public Result<String> delete(Long[] ids) {
        typeMappingService.removeBatchByIds(Arrays.asList(ids));

        return Result.ok();
    }
}