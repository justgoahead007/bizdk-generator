package com.bizdk.generator.controller.gen;

import com.bizdk.generator.common.page.PageResult;
import com.bizdk.generator.common.query.Query;
import com.bizdk.generator.common.utils.Result;
import com.bizdk.generator.controller.BaseController;
import com.bizdk.generator.entity.TableEntity;
import com.bizdk.generator.entity.TableColumnEntity;
import com.bizdk.generator.service.TableColumnService;
import com.bizdk.generator.service.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 数据表管理
*/
@Controller
@RequestMapping("/gen/table")
public class TableController extends BaseController {
    @Autowired
    private TableService tableService;

    @Autowired
    private TableColumnService tableColumnService;

    @Override
    protected String getPrefix() {
        return "gen/table";
    }

    /**
     * 分页
     *
     * @param query 查询参数
     */
    @ResponseBody
    @RequestMapping("/page")
    public Result<PageResult<TableEntity>> page(Query query) {
        PageResult<TableEntity> page = tableService.page(query);

        return Result.ok(page);
    }

    @Override
    @GetMapping("/edit/{id}")
    public Object edit(@PathVariable("id") Long id) {
        TableEntity data = tableService.getById(id);
        // 获取表的字段
        List<TableColumnEntity> columnEntityList = tableColumnService.getByTableId(data.getId());
        data.setColumnEntities(columnEntityList);

        ModelAndView modelAndView = new ModelAndView(super.edit(id).toString());
        modelAndView.addObject("table", data);
        modelAndView.addObject("tableId", id);
        return modelAndView;
    }

    /**
     * 修改
     *
     * @param table 表信息
     */
    @ResponseBody
    @PutMapping("/update")
    public Result<String> update(TableEntity table) {
        tableService.updateById(table);

        return Result.ok();
    }

    /**
     * 删除
     *
     * @param ids 表id数组
     */
    @ResponseBody
    @PostMapping("/delete")
    public Result<String> delete(Long[] ids) {
        tableService.deleteBatchIds(ids);

        return Result.ok();
    }

    /**
     * 同步表结构
     *
     * @param id 表ID
     */
    @ResponseBody
    @PostMapping("/sync/{id}")
    public Result<String> sync(@PathVariable("id") Long id) {
        tableService.sync(id);
        return Result.ok();
    }


    @ResponseBody
    @GetMapping("/list/{datasourceId}")
    public Result<List<TableEntity>> list(@PathVariable("datasourceId") Long datasourceId) {
        List<TableEntity> tableEntities = tableService.tableList(datasourceId);
        return Result.ok(tableEntities);
    }


    /**
     * 导入数据源中的表
     *
     * @param datasourceId  数据源ID
     * @param tableNameList 表名列表
     */
    @ResponseBody
    @PostMapping("/import/{datasourceId}")
    public Result<String> tableImport(@PathVariable("datasourceId") Long datasourceId, @RequestParam List<String> tableNameList) {
        for (String tableName : tableNameList) {
            tableService.tableImport(datasourceId, tableName);
        }

        return Result.ok();
    }

    /**
     * 修改表字段数据
     *
     * @param tableId        表ID
     * @param table 字段列表
     */
    @ResponseBody
    @PutMapping("/column/{tableId}")
    public Result<String> updateTableColumn(@PathVariable("tableId") Long tableId, @RequestBody TableEntity table) {
        tableService.updateById(table);
        tableColumnService.updateTableColumn(tableId, table.getColumnEntities());
        return Result.ok();
    }

    @GetMapping("/gen")
    public Object gen(@RequestParam("tableIds") Long[] tableIds) {
        
        String[] idArr = new String[tableIds.length];
        for (int i = 0; i < idArr.length; i++) {
            idArr[i] = tableIds[i]+"";
        }
        ModelAndView modelAndView = new ModelAndView(getPrefix() + "/gen");
        modelAndView.addObject("tableIds", idArr);
        return modelAndView;
    }

}
